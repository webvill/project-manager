### Project manager
**Server** - dotnet minimal webapi

**Api** - full dotnet api with controllers
```
cd Api
dotnet ef database update --context DataContext
dotnet run -> database will seed
```
**client** - react app
```
npm install
```
Login to be able to create a new project
```
username: Admin
password: Admin
```
