namespace Api.Models.Users;
using Api.Entities;


public class AuthenticateResponse
{
    public User User { get; set; }
    public string Token { get; set; }
}