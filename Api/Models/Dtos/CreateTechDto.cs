namespace Api.Models.Dtos
{
    public class CreateTechDto
    {
        public string Name { get; set; }
        public List<int> Projects { get; set; }
    }
}