namespace Api.Entities
{
    public class CreateTodoDto
    {
        public string Body { get; set; } = string.Empty;
        public int Priority { get; set; } = 3;
        public bool Done { get; set; } = false;
    }
}