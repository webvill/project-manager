using Api.Entities;

namespace Api.Models.Dtos
{
    public class ProjectDto
    {
        public int Id { get; set; }
        public string Name { get; set; }  = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Log { get; set; } = string.Empty;
        public string Path { get; set; } = string.Empty;
        public int CategoryId { get; set; }
        public List<Tech> Tech { get; set; }
        public List<Log> Logs  { get; set; }
        public List<Todo> Todos { get; set; }
         public string CreatedAt { get; set; } = string.Empty;
        public string UpdatedAt { get; set; } = string.Empty;
    }
}