using System.ComponentModel.DataAnnotations;

namespace Api.Entities
{
    public class DocumentDto
    {
        [Required]
        public string Title { get; set; } = string.Empty;
        [Required]
        public string Body { get; set; } = string.Empty;
        public string Path { get; set; } = string.Empty;
    }
}