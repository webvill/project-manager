namespace Api.Models.Dtos
{
    public class CreateProjectDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public int CategoryId { get; set; }
        public List<int> Tech { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
    }
}