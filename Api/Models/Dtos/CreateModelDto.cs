using System.ComponentModel.DataAnnotations;

namespace Api.Entities
{
    public class CreateModelDto
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public List<int> Categories { get; set; }
    }
}