using Api.Entities;

namespace Api.Models.Dtos
{
    public class TechDto
    {
        public int Id { get; set; }
        public string Name { get; set; }  = string.Empty;
        public List<Document> Documents { get; set; }
    }
}