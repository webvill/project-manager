namespace Api.Models.Dtos
{
    public class CreateLogDto
    {
        public string Day { get; set; }
        public string Body { get; set; }
    }
}