#nullable disable
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Api.Entities;
using Api.Repository;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodosController : ControllerBase
    {
        private readonly ITodoRepository _service;

        public TodosController(ITodoRepository service)
        {
            _service = service;
        }
         [HttpGet("{id}")]
        public async Task<ActionResult<Todo>> GetProject(int id)
        {
            var todo = await _service.GetById(id);
            
            if (todo == null)
            {
                return NotFound();
            }
            return todo;
        }

        
        // PUT: api/Todos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodo(int id, Todo todo)
        {
            if (id != todo.Id)
            {
                return BadRequest();
            }


            try
            {
                await _service.UpdateAsync(id, todo);
            }
            catch (DbUpdateConcurrencyException)
            {
               
               return NotFound();
            }

            return NoContent();
        }

        

        // DELETE: api/Todos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodo(int id)
        {
            Console.WriteLine("delete");
            var todo = await _service.GetById(id);
            if (todo == null)
            {
                return NotFound();
            }

            await _service.Delete(id);

            return NoContent();
        }

      
    }
}
