#nullable disable
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Api.Entities;
using Api.Models.Dtos;
using Api.Repository;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StacksController : ControllerBase
    {
        private readonly IStackRepository _service;

        public StacksController(IStackRepository service)
        {
            _service = service;
        }

        // GET: api/Techs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Tech>>> GetTech()
        {
            var reponse = await _service.GetAll(n => n.Documents);
            return Ok(reponse);

        }

        // GET: api/Techs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Tech>> GetTech(int id)
        {
            var tech = await _service.GetById(id, tech => tech.Documents);

            if (tech == null)
            {
                return NotFound();
            }

            return tech;
        }

        // PUT: api/Techs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTech(int id, Tech tech)
        {
            if (id != tech.Id)
            {
                return BadRequest();
            }


            try
            {
              await _service.UpdateAsync(id, tech);

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TechExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        
        // POST: api/Techs/5/documents
        [HttpPost("{id}/documents")]
        public async Task<ActionResult> PostTech(int id, DocumentDto doc)
        {
            var document = new Document
            {
                TechId = id,
                Title = doc.Title,
                Body = doc.Body,
                Path = doc.Path
            };
            await _service.AddDocument(document);
            

           
            return Ok(document);
        }

        // POST: api/Techs
        [HttpPost]
        public async Task<ActionResult<Tech>> PostTech(CreateTechDto dto)
        {
            var tech = new Tech { Name = dto.Name };
            await _service.Add(tech);

            return CreatedAtAction("GetTech", new { id = tech.Id }, tech);
        }

        // DELETE: api/Techs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTech(int id)
        {
            var tech = await _service.GetById(id);
            if (tech == null)
            {
                return NotFound();
            }

            await _service.Delete(id);
            return NoContent();
        }
        
        private bool TechExists(int id)
        {
            return _service.TechExists(id);
            
        }

       

        private void WriteDocumentToFile(string fileName, string body)
        {
             // Set a variable to the Documents path.
            string docPath =
            Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            
            
            // Write 
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, fileName)))
            {
                /* foreach (string line in lines)
                    outputFile.WriteLine(line); */
                    outputFile.Write(body);
            }
        }
    }
}
