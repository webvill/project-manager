#nullable disable
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Api.Entities;
using Api.Models.Dtos;
using Api.Repository;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentsController : ControllerBase
    {
        private readonly IDocumentRepository _service;

        public DocumentsController(IDocumentRepository service)
        {
            _service = service;
        }

        

        [HttpGet("{id}")]
        public async Task<ActionResult<Tech>> GetDocument(int id)
        {
            var doc = await _service.GetById(id);

            if (doc == null)
            {
                return NotFound();
            }

            return Ok(doc);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutDocument(int id, Document doc)
        {
            if (id != doc.Id)
            {
                return BadRequest();
            }


            try
            {
              await _service.UpdateAsync(id, doc);

            }
            catch (DbUpdateConcurrencyException)
            {
                
                    return NotFound();
        
            }

            return NoContent();
        }

           
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDocument(int id)
        {
            var doc = await _service.GetById(id);
            if (doc == null)
            {
                return NotFound();
            }

            await _service.Delete(id);
            return NoContent();
        }
        
        

        private void WriteDocumentToFile(string fileName, string body)
        {
             // Set a variable to the Documents path.
            string docPath =
            Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            
            
            // Write 
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, fileName)))
            {
                /* foreach (string line in lines)
                    outputFile.WriteLine(line); */
                    outputFile.Write(body);
            }
        }
    }
}
