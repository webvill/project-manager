#nullable disable
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Api.Entities;
using Api.Authorization;
using Api.Models.Dtos;
using Api.Repository;
using AutoMapper;

namespace Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectRepository _service;
        private readonly IMapper _mapper;
        private ILoggerManager _logger;

        public ProjectsController(ILoggerManager logger, IProjectRepository service, IMapper mapper)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
            
        }

        // GET: api/Projects
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDto>>> GetProjects()
        {
            var projects = await _service.GetAll(s => s.Tech, s => s.Logs);
            var projectDtos =_mapper.Map<IEnumerable<ProjectDto>>(projects);
            return Ok(projectDtos);
            // return await _context.Projects.Include(project => project.Tech)
            //     .Select(p => new ProjectDto {
            //         Id = p.Id, 
            //         Name = p.Name, 
            //         Description = p.Description, 
            //         Path = p.Path, 
            //         CategoryId = p.CategoryId,
            //         Tech = p.Tech
            //     }).ToListAsync();
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDto>> GetProject(int id)
        {
            var project = await _service.GetById(id, p => p.Tech, p => p.Logs, p => p.Todos);
            // var project = await _context.Projects
            //     .Include(project => project.Tech)
            //     .Include(project => project.Logs)
            //     .FirstOrDefaultAsync(project => project.Id == id);

            if (project == null)
            {
                return NotFound();
            }

            var projectDto = _mapper.Map<ProjectDto>(project);
            return projectDto;
        }


        // GET: api/Projects/5/Todos
        [HttpGet("{id}/todos")]
        public async Task<ActionResult<IEnumerable<Todo>>> GetProjectTodos(int id)
        {
            var todos = await _service.GetProjectTodos(id);

            return Ok(todos);
        }

        // PUT: api/Projects/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProject(int id, CreateProjectDto dto)
        {
            var existingProject = await _service.GetById(id, project => project.Tech);

            if (existingProject == null)
            {
                return BadRequest();
            }
            
            existingProject.Name = dto.Name;
            existingProject.Description = dto.Description;
            existingProject.Path = dto.Path;
            existingProject.CategoryId = dto.CategoryId;
            existingProject.UpdatedAt = DateTimeOffset.UtcNow;
            var updatedTech = dto.Tech;

            try
            {
                await _service.PutProject(existingProject, updatedTech);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        // POST: api/Projects
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<Project>> PostProject(CreateProjectDto dto)
        {
            
            var user = (User)HttpContext.Items["User"];
           
            var project = await _service.PostProject(user.Id, dto);
            return CreatedAtAction("GetProject", new { id = project.Id }, project);
        }

        // GET: api/Projects/5/logs
        [HttpGet("{id}/logs")]
        public async Task<ActionResult<Log>> GetProjectLogs(int id)
        {
            var logs = await _service.GetProjectLogs(id);
            return Ok(logs);
        }

        // POST: api/Projects/id/log
        [HttpPost("{id}/log")]
        public async Task<ActionResult<Log>> PostLog(int id, CreateLogDto dto)
        {
            var project = _service.GetById(id);
            if (project == null)
            {
                return BadRequest();
            }
            var log = new Log
            {
                ProjectId = id,
                Day = dto.Day,
                Body = dto.Body
            };

            await _service.PostLog(log);
            
            return Ok(log);
        }

        [HttpPost("{id}/todos")]
        public async Task<ActionResult<Todo>> PostTodo(int id, CreateTodoDto dto)
        {
            
            // check if project exists?
            //var project = _service.GetById(id);
            // if (project == null)
            // {
            //     return NotFound();
            // }
            var todo = new Todo()
            {
                ProjectId = id,
                Body = dto.Body,
                Priority = dto.Priority,
                Done = false
            };
            await _service.PostTodo(todo);
            return Ok(todo);
            //return CreatedAtAction("GetTodo", new { id = todo.Id }, todo);
        }
        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProject(int id)
        {
           await _service.Delete(id);
            return NoContent();
        }

        
    }
}
