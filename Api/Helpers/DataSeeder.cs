using Api.Entities;

namespace Api.Helpers
{
using BCrypt.Net;
    public class DataSeeder
    {
        private readonly DataContext _context;
        

        public DataSeeder(DataContext context)
        {
            _context = context;
        }

        public void Seed()
        {
    
           if(! _context.Categories.Any())
            {
                var categories = new List<Category>(){ new Category(){Name = "Webbutveckling"} };
                _context.Categories.AddRange(categories);
                _context.SaveChanges();
            }
            if (!_context.Tech.Any())
            {
                var tech = new List<Tech>()
                {
                    new Tech(){ Name = ".Net" },
                    new Tech(){ Name = "React.js" },
                    new Tech(){ Name = "SqlServer" },
                    new Tech(){ Name = "MongoDB" },
                    new Tech(){ Name = "Node" },
                    new Tech(){ Name = "Express" },
                    new Tech(){ Name = "PostgreSQL" },
                    new Tech(){ Name = "Docker" },
                };
                _context.Tech.AddRange(tech);
                _context.SaveChanges();
            }   
            if (!_context.Users.Any())
            {
                
                var user = new User()
                {
                    FirstName = "Admin",
                    LastName = "Admin",
                    Username = "Admin",
                    PasswordHash = BCrypt.HashPassword("Admin")
                };
                _context.Users.Add(user);
                _context.SaveChanges();
            } 
            if (!_context.Projects.Any())
            {
                var project = new Project()
                {
                    Name = "Project Manager",
                    Description = "Keep track of your projects",
                    Path = "https://bitbucket.org/webvill/project-manager/src/master",
                    CategoryId = _context.Categories.Where(c => c.Name == "Webbutveckling").FirstOrDefault().Id,
                    UserId = _context.Users.Where(u => u.FirstName == "Admin").FirstOrDefault().Id,
                    Tech = new List<Tech>()
                    { 
                        _context.Tech.Where(t => t.Name == ".Net").FirstOrDefault(),
                        _context.Tech.Where(t => t.Name == "React.js").FirstOrDefault()
                    },
                    CreatedAt = DateTimeOffset.UtcNow,
                    UpdatedAt = DateTimeOffset.UtcNow,

                };
                _context.Projects.Add(project);
                _context.SaveChanges();
            }

            }
        
        }
    }
