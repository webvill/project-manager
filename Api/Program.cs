using Api.Entities;
using Api.Helpers;
using Api.Services;
using Api.Authorization;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using System.Text.Json.Serialization;
using NLog;
using Api.Repository;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;

// Add services to the container.

LogManager.LoadConfiguration(string.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));

services.AddControllers().AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
services.AddEndpointsApiExplorer();
services.AddSwaggerGen();

services.ConfigureCors();
services.ConfigureIISIntegration();
services.ConfigureLoggerService();

// if (builder.Environment.IsDevelopment())
//     services.AddDbContext<DataContext, MysqlDataContext>();
// else 
    services.AddDbContext<DataContext>();

services.AddTransient<DataSeeder>();

#region Repositories
services.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));
services.AddTransient<IModelRepository, ModelRepository>();
#endregion
services.AddTransient<IUnitOfWork, UnitOfWork>();

services.AddAutoMapper(typeof(Program));

services.Configure<AppSettings>(builder.Configuration.GetSection("AppSettings"));

services.AddScoped<IJwtUtils, JwtUtils>();
services.AddScoped<IUserService, UserService>();
services.AddScoped<IProjectRepository, ProjectRepository>();
services.AddScoped<IStackRepository, StackRepository>();
services.AddScoped<IDocumentRepository, DocumentRepository>();
services.AddScoped<ICategoryRepository, CategoryRepository>();
services.AddScoped<ITodoRepository, TodoRepository>();

var app = builder.Build();
    
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseDeveloperExceptionPage();;
}
else 
{
    app.UseHsts();
}
var scopedFactory = app.Services.GetService<IServiceScopeFactory>();
using (var scope = scopedFactory.CreateScope())
{
    var seeder = scope.ServiceProvider.GetService<DataSeeder>();
    seeder.Seed();
}

app.UseStaticFiles();

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.All
});
app.UseCors("CorsPolicy");

app.UseHttpsRedirection();

app.UseAuthorization();

//app.UseMiddleware<ErrorHandlerMiddleware>();
    
app.UseMiddleware<JwtMiddleware>();

app.MapControllers();

app.Run();
