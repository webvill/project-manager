namespace Api.Entities
{
    public class Document : EntityBase
    {
        public int TechId { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Body { get; set; } = string.Empty;
        public string Path { get; set; } = string.Empty;
    }
}