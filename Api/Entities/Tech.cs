using System.Text.Json.Serialization;

namespace Api.Entities
{
    public class Tech :EntityBase
    {
        public string Name { get; set; } = string.Empty;
        [JsonIgnore]
        public List<Project> Projects { get; set; }
        public List<Document> Documents { get; set; }
    }
}