namespace Api.Entities
{
    public enum Priority{High = 1, Medium = 2, Low = 3};
    public class Todo: EntityBase
    {
        public int ProjectId { get; set; }
        public string Body { get; set; } = string.Empty;
        public int Priority { get; set; } = 3;
        public bool Done { get; set; } = false;
    }
}