namespace Api.Entities
{
    public class Log:EntityBase
    {
        public int ProjectId { get; set; }
        public string Day { get; set; }
        public string Body { get; set; }
    }
}