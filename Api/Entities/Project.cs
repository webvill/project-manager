using System.Text.Json.Serialization;

namespace Api.Entities
{
    public class Project: EntityBase
    {

        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Path { get; set; } = string.Empty;
        public int CategoryId { get; set; }
        public int UserId { get; set; }
        [JsonIgnore]
        public Category Category { get; set; }
        public List<Tech> Tech { get; set; }
        public List<Todo> Todos { get; set; }
        public List<Log> Logs { get; set; }
        public DateTimeOffset CreatedAt { get; set; } 
        public DateTimeOffset UpdatedAt { get; set; }
    }
}