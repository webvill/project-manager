using Api.Repository;

namespace Api.Entities;
public class EntityBase:IEntityBase
{
    public int Id { get; set; }
}