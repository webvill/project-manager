namespace Api.Entities
{
    public class Model:EntityBase
    {
        public string Name { get; set; } = string.Empty;
        public List<Category> Categories { get; set; }
    }
}