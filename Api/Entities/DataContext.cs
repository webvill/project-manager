using Microsoft.EntityFrameworkCore;

namespace Api.Entities
{
    public class DataContext : DbContext
    {
        protected readonly IConfiguration Configuration;

        public DataContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));

        } 
        
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }/* => Set<Project>(); */
        public DbSet<Category> Categories { get; set; }/* => Set<Category>(); */
        public DbSet<Tech> Tech { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Todo> Todos { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Model> Models { get; set; }
    }
}