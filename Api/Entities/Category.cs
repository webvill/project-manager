using System.Text.Json.Serialization;

namespace Api.Entities
{
    public class Category : EntityBase
    {
        public string Name { get; set; }
        [JsonIgnore]
        public List<Project> Projects { get; set; }
    }
}