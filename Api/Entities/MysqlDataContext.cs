using Microsoft.EntityFrameworkCore;

namespace Api.Entities
{
    public class MysqlDataContext : DataContext
    {

        public MysqlDataContext(IConfiguration configuration) : base(configuration)
        {
        }


        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var connectionString = Configuration.GetConnectionString("MyProjectManager"); 
            //options.UseMySql(connectionString, new MariaDbServerVersion(new Version(10, 3, 12)));
        } 
        
    }
}