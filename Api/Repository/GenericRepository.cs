namespace Api.Repository;
using System.Linq.Expressions;
using Api.Entities;
using Microsoft.EntityFrameworkCore;

public class GenericRepository<T> : IGenericRepository<T> where T : class, IEntityBase, new()
{
   private readonly DataContext _context;
   private readonly DbSet<T> dbSet;
   public GenericRepository(DataContext context)
   {
       _context = context;
       dbSet = _context.Set<T>();
   }

   public async Task<bool> Add(T entity)
   {
       await dbSet.AddAsync(entity);
       await _context.SaveChangesAsync();
       return true;
   }

   public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
   {
       return dbSet.Where(expression);
   }

   public async Task<IEnumerable<T>> GetAll()
   {
        return await dbSet.ToListAsync();

    }
    public async Task<IEnumerable<T>> GetAll(string[] paths = null)
    {
        return await paths.Aggregate(dbSet.AsQueryable(), (query, path) => query
            .Include(path)).ToListAsync();

    }
    public async Task<IEnumerable<T>> GetAll(params Expression<Func<T, object>>[] includes)
    {
        IQueryable<T> query = dbSet;
        query = includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        return await query.ToListAsync();

    }

    public async Task<T> GetById(int id)
    {
        return await dbSet.FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<T> GetById(int id, string[] includes)
    {
       var model = await dbSet.FindAsync(id);
       foreach (var path in includes)
       {
          _context.Entry(model).Reference(path).Load();
       }
       return model;
    }
    public async Task<T> GetById(int id, params Expression<Func<T, object>>[] includes)
    {
        IQueryable<T> query = dbSet;
        query = includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        return await query.FirstOrDefaultAsync(n => n.Id == id);
    }

   public async Task<bool> Delete(int id)
   {
            var entity = await dbSet.FirstOrDefaultAsync(n => n.Id == id);
            _context.Entry<T>(entity).State = EntityState.Deleted;

            return entity != null ? true : false; 
    //    var t = await dbSet.FindAsync(id);

    //    if (t != null)
    //    {
    //        dbSet.Remove(t);
    //        return true;
    //    }
    //    else
    //        return false;
   }
    public async Task UpdateAsync(int id, T entity)
    {
        _context.Entry<T>(entity).State = EntityState.Modified;

        await _context.SaveChangesAsync();
    }
   public Task<bool> Upsert(T entity)
   {
       throw new NotImplementedException();
   }

    public void AddRange(IEnumerable<T> entities)
    {
        throw new NotImplementedException();
    }

    public void Range(IEnumerable<T> entities)
    {
        throw new NotImplementedException();
    }
}