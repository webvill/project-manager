namespace Api.Repository;
public interface IUnitOfWork: IDisposable
{
    IModelRepository Models { get; }
    Task<int> CompletedAsync();
}