using Api.Entities;

namespace Api.Repository;
public class DocumentRepository: GenericRepository<Document>, IDocumentRepository
{
    public DocumentRepository(DataContext context): base(context)
    {
        
    }
    
}