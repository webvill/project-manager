using Api.Entities;

namespace Api.Repository;
public class CategoryRepository: GenericRepository<Category>, ICategoryRepository
{
    public CategoryRepository(DataContext context): base(context)
    {
        
    }
    
}