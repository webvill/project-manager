using Api.Entities;
using Api.Models.Dtos;

namespace Api.Repository;
public class ProjectRepository: GenericRepository<Project>, IProjectRepository
{
    private readonly DataContext _context;
    public ProjectRepository(DataContext context) : base(context) => _context = context;

    public async Task<IEnumerable<Log>> GetProjectLogs(int id)
    {
        var project = await GetById(id, project => project.Logs);
        return project.Logs;
    }
    public async Task<IEnumerable<Todo>> GetProjectTodos(int id)
    {
       var project = await GetById(id, project => project.Todos);
        return project.Todos;
    }
    public async Task PutProject(Project existingProject, List<int> updatedTech)
    {
            existingProject.Tech.Clear();
            
            existingProject.Tech = _context.Tech.Where(item => updatedTech.Contains(item.Id)).ToList();
            
            await _context.SaveChangesAsync();

    }
    public async Task PostLog(Log log)
    {
         _context.Logs.Add(log);
         await _context.SaveChangesAsync();  
    }
    public async Task PostTodo(Todo todo)
    {
        _context.Todos.Add(todo);
         await _context.SaveChangesAsync();  
    }

    public async Task<Project> PostProject(int userId, CreateProjectDto dto)
    {
         var project = new Project
        {
            Name = dto.Name,
            Description = dto.Description,
            Path = dto.Path,
            CategoryId = dto.CategoryId,
            UserId = userId,
            Tech = _context.Tech.Where(item => dto.Tech.Contains(item.Id)).ToList(),
            CreatedAt = DateTimeOffset.UtcNow,
            UpdatedAt = DateTimeOffset.UtcNow

        };
        _context.Projects.Add(project);
        await _context.SaveChangesAsync();
        return project;
    }
}