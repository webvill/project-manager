﻿namespace Api.Repository;
public interface IEntityBase
{
    int Id { get; set; }
}
