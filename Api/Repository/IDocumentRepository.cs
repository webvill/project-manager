using Api.Entities;

namespace Api.Repository;
public interface IDocumentRepository: IGenericRepository<Document>
{
}