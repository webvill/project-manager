using Api.Entities;

namespace Api.Repository;
public interface ICategoryRepository: IGenericRepository<Category>
{
}