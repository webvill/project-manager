using Api.Entities;

namespace Api.Repository;
public interface ITodoRepository: IGenericRepository<Todo>
{
    
}