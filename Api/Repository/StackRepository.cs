using Api.Entities;

namespace Api.Repository;
public class StackRepository : GenericRepository<Tech>, IStackRepository
{
    private readonly DataContext _context;
    public StackRepository(DataContext context): base(context)
    {
        _context = context;   
    }
    
    public async Task AddDocument(Document document)
    {
        
        await _context.Documents.AddAsync(document);
        await _context.SaveChangesAsync();
    

    }
    public bool TechExists(int id)
    {
        return  _context.Tech.Any(e => e.Id == id);

    }
}