using Api.Entities;
using Api.Models.Dtos;

namespace Api.Repository;
public interface IProjectRepository: IGenericRepository<Project>
{
    Task<IEnumerable<Todo>> GetProjectTodos(int id);
    Task<IEnumerable<Log>> GetProjectLogs(int id);
    Task<Project> PostProject(int projectId, CreateProjectDto dto);
    Task PutProject(Project existingProject, List<int> updatedTech);
    Task PostTodo(Todo todo);
    Task PostLog(Log log);
}
