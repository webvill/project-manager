using Api.Entities;

namespace Api.Repository;
public interface IStackRepository: IGenericRepository<Tech>
{
    Task AddDocument(Document document);
    bool TechExists(int id);
}