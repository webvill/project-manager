using Api.Entities;

namespace Api.Repository;
public class UnitOfWork: IUnitOfWork
{
    private readonly DataContext _context;
    private readonly ILogger _logger;
    public IModelRepository Models { get; private set; }

    public UnitOfWork(
        DataContext context,
        ILoggerFactory logger
        )
    {
        _context = context;
        _logger = logger.CreateLogger("logs");

        Models = new ModelRepository(_context);
    }

    public async Task<int> CompletedAsync()
    {
        return await _context.SaveChangesAsync();
    }

    public void Dispose()
    {
        _context.Dispose();
    }
}
