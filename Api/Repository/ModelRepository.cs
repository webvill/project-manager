namespace Api.Repository;
using Api.Entities;

public class ModelRepository: GenericRepository<Model>, IModelRepository
{
    public ModelRepository(DataContext context) : base(context)
    {
    }
}