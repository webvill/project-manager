using Api.Entities;

namespace Api.Repository;
public class TodoRepository: GenericRepository<Todo>, ITodoRepository
{
    public TodoRepository(DataContext context): base(context)
    {
        
    }
    
}