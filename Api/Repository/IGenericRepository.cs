using System.Linq.Expressions;

namespace Api.Repository;
public interface IGenericRepository<T> where T : class
{
    Task<T> GetById(int id, string[] includes);
    Task<T> GetById(int id, params Expression<Func<T, object>>[] includes);
    Task<IEnumerable<T>> GetAll();
    Task<IEnumerable<T>> GetAll(string[] includes);
    Task<IEnumerable<T>> GetAll(params Expression<Func<T, object>>[] includes);
    IEnumerable<T> Find(Expression<Func<T, bool>> expression);
    Task<bool> Add(T entity);
    void AddRange(IEnumerable<T> entities);
    Task<bool> Delete(int id);
    void Range(IEnumerable<T> entities);
    Task UpdateAsync(int id, T entity);
    Task<bool> Upsert(T entity);
}