using Api.Entities;

namespace Api.Repository;

public interface IModelRepository: IGenericRepository<Model>
{
}