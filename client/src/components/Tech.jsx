import { useEffect, useState } from "react";
import {
   Button,
   NavLink,
   Modal,
   Card,
   Row,
   Col,
   ListGroup,
} from "react-bootstrap";
import { PencilFill, Trash } from "react-bootstrap-icons";
import ReactMarkdown from "react-markdown";

import { useParams } from "react-router-dom";
import DocumentForm from "./DocumentForm";
import EditDocumentForm from "./EditDocumentForm";

const Tech = () => {
   const [tech, setTech] = useState({ documents: [] });
   const [showModal, setShowModal] = useState(false);
   const [document, setDocument] = useState({});
   const [editDocMode, setEditDocMode] = useState(false);
   const { id } = useParams();

   // fetch tech/stack
   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(
            `https://localhost:7000/api/stacks/${id}`
         );
         const data = await response.json();
         console.log("data", data);
         setTech(data);
         setDocument(
            data.documents.length > 0
               ? data.documents[0]
               : {
                    title: "",
                    filePath: "",
                    body: "",
                 }
         );
      };

      fetchData();
   }, []);

   const toggleModal = () => {
      setShowModal((showModal) => !showModal);
   };

   function handleEdit(id) {
      setEditDocMode(editDocMode => !editDocMode);
      setDocument(tech.documents.find((doc) => doc.id === id));
   }
   const handleUpdate = async (id) => {
      const response = await fetch(
         `https://localhost:7000/api/documents/${document.id}`,
         {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(document),
         }
      );
      
      setTech({
         ...tech,
         documents: tech.documents.map(doc => doc.id == document.id ? document : doc ),
      });

   }
   const handleDelete = async (e, id) => {
      e.preventDefault();
      await fetch(`https://localhost:7000/api/documents/${id}`, {
         method: "DELETE",
      });
      setEditDocMode(false);
      setTech({
         ...tech,
         documents: tech.documents.filter((doc) => doc.id !== id),
      });
   };
   return (
      <>
         <Row>
            <Col md={4}>
               <h3>{tech.name}</h3>
               <div className="d-flex justify-content-between mb-1">
                  <span className="fs-5">Documents</span>
                  <Button
                     size="sm"
                     onClick={toggleModal}
                     variant="outline-primary"
                  >
                     Add document
                  </Button>
               </div>
               <ListGroup>
                  {tech.documents.map((doc) => (
                     <ListGroup.Item
                        key={doc.id}
                        className="d-flex justify-content-between align-items-center ps-0 pe-1"
                     >
                        <NavLink onClick={() => {
                           setDocument(doc)
                           setEditDocMode(false)
                           }}>
                           {doc.title}
                        </NavLink>
                        <div className="d-flex justify-content-between">
                           <Button
                              size="sm"
                              variant="outline-dark"
                              className="me-3"
                              onClick={(id) => handleEdit(doc.id)}
                           >
                              <PencilFill
                              />
                           </Button>
                           <Button onClick={(e) => handleDelete(e, doc.id)} size="sm" variant="outline-danger">
                              <Trash
                                 
                              />
                           </Button>
                        </div>
                     </ListGroup.Item>
                  ))}
               </ListGroup>
            </Col>
            <Col>
               {tech.documents.length > 0 && !editDocMode && (
                  <>
                     <Card>
                        <Card.Body>
                           <Card.Title>{document.title}</Card.Title>
                           <Card.Text>File: {document.filePath}</Card.Text>
                           <ReactMarkdown>{document.body}</ReactMarkdown>
                        </Card.Body>
                     </Card>
                  </>
               )}
               {tech.documents.length > 0 && editDocMode && (
                  <>
                     <EditDocumentForm
                        document={document}
                        setDocument={setDocument}
                        setEditDocMode={setEditDocMode}
                        handleUpdate={handleUpdate}
                     />
                  </>
               )}
            </Col>
         </Row>
         <Modal show={showModal} onHide={toggleModal}>
            <Modal.Header closeButton>
               <Modal.Title className="text-center">
                  Create a new document
               </Modal.Title>
            </Modal.Header>
            <Modal.Body>
               <Card>
                  <Card.Body>
                     <DocumentForm
                        tech={tech}
                        setTech={setTech}
                        toggleModal={toggleModal}
                        id={id}
                     />
                  </Card.Body>
               </Card>
            </Modal.Body>
            <Modal.Footer></Modal.Footer>
         </Modal>
      </>
   );
};

export default Tech;
