import { useState, useEffect } from "react";
import { Form, Button, Card } from "react-bootstrap";
import StackCard from "./StackCard";
const StackForm = () => {
   const [stack, setStack] = useState({ id: 0, name: "" });
   const [editMode, setEditMode] = useState(false);
   const [name, setName] = useState("");
   const [stacks, setStacks] = useState([]);

   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(`https://localhost:7000/api/stacks`);
         const data = await response.json();
         console.log("data", data);
         setStacks(data);
      };

      fetchData();
   }, []);
   const onChange = (e) => {
      setStack({ ...stack, name: e.target.value });
      setName(e.target.value);

      console.log(name);
   };
   const handleUpdate = async (e) => {
      e.preventDefault();
      try {
         await fetch(`https://localhost:7000/api/stacks/${stack.id}`, {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(stack),
         });
         setStack({id: 0, name:""})
         setEditMode(false);
         setStacks(
            stacks.map((item) =>
               item.id === stack.id ? { ...item, name: stack.name } : item
            )
         );
      } catch (error) {}
   };

   const handleSubmit = async (e) => {
      e.preventDefault();
      try {
         const response = await fetch(`https://localhost:7000/api/stacks`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ name: stack.name, projects: [] }),
         });
         const data = await response.json();
         console.log('data', data)
         setStacks([...stacks, data]);
         setStack({id: 0, name:""})
      } catch (error) {}
   };

   const handleDelete = async (id) => {
      
      try {
         await fetch(`https://localhost:7000/api/stacks/${id}`, {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(stack),
         });
         setStacks(stacks.filter(item => item.id !== id))
         
      } catch (error) {}
   }
   return (
      <Card>
         <Card.Body>
            <Card.Title>Create a new stack</Card.Title>
            <Form>
               <Form.Group className="mb-3" controlId="name">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                     name="name"
                     value={stack.name}
                     onChange={onChange}
                     type="text"
                     placeholder="Stack name"
                  />
               </Form.Group>
               <StackCard
                  stack={stack}
                  setStack={setStack}
                  stacks={stacks}
                  name={name}
                  handleDelete={handleDelete}
                  editMode={editMode}
                  setEditMode={setEditMode}
               />
               <div className="d-grid">
                  <Button
                     onClick={editMode ? handleUpdate : handleSubmit}
                     variant="outline-primary"
                     type="submit"
                  >
                     {editMode ? "Update" : "Create"}
                  </Button>
               </div>
            </Form>
         </Card.Body>
      </Card>
   );
};

export default StackForm;
