import { useEffect, useState } from "react";
import { ListGroup, Form, Button, Card, Row, Col } from "react-bootstrap";
import { CaretRightFill, CircleFill, Trash } from "react-bootstrap-icons";
const Todo = ({ id }) => {
   const [todos, setTodos] = useState([]);
   const [body, setBody] = useState("");
   const [priority, setPriority] = useState(1);
   const [todoId, setTodoId] = useState();
   const [todoDone, setTodoDone] = useState();
   const [showCompleted, setShowCompleted] = useState(true);
   const [updateMode, setUpdateMode] = useState(false);

   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(
            `https://localhost:7000/api/projects/${id}/todos`
         );
         const data = await response.json();
         console.log("data", data);
         setTodos(data);
      };

      fetchData();
   }, []);

   const onChange = (e) => {
      if (e.target.name === "body") {
         setBody(e.target.value);
      } else {
         setPriority(e.target.value);
      }
   };
   const handleSubmit = async (e) => {
      e.preventDefault();
      const response = await fetch(`https://localhost:7000/api/projects/${id}/todos`, {
         method: "POST",
         headers: { "Content-Type": "application/json" },
         body: JSON.stringify({ body, priority }),
      });
      const data = await response.json();
      console.log("data", data);
      setTodos([...todos, data]);
      setBody("");
   };

   const handleUpdate = async (e) => {
      e.preventDefault();
      const response = await fetch(
         `https://localhost:7000/api/projects/${todoId}/todos`,
         {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
               id: todoId,
               projectId: id,
               body,
               priority,
               done: todoDone,
            }),
         }
      );
      setTodos(
         todos.map((todo) =>
            todo.id === todoId ? { ...todo, body, priority } : todo
         )
      );
      setBody("");
      setPriority(1);
      setUpdateMode(false);
   };
   const handleDelete = async (todoId) => {
      const response = await fetch(
         `https://localhost:7000/api/todos/${todoId}`,
         {
            method: "DELETE",
         }
      );
      // const data = await response.json();
      // console.log("data", data);
      setTodos(todos.filter((todo) => todo.id !== todoId));
   };

   const toggleCompleted = async (e) => {
      e.preventDefault();
      const id = parseInt(e.target.value);
      const updatedTodos = todos.map((todo) => {
         if (todo.id === id) {
            return { ...todo, done: !todo.done };
         }
         return todo;
      });

      const todoToUpdate = updatedTodos.find((todo) => todo.id === id);
      await fetch(`https://localhost:7000/api/projects/${id}`, {
         method: "PUT",
         headers: { "Content-Type": "application/json" },
         body: JSON.stringify(todoToUpdate),
      });
      setTodos(updatedTodos);
      //setTodos(todos.filter((todo) => todo.id !== id));
   };

   const todoListItems = (todo) => {
      return (
         <ListGroup.Item
            className="d-flex justify-content-between px-1"
            key={todo.id}
            style={{
               borderLeft: `3px solid ${
                  todo.priority === 1
                     ? "mediumseagreen"
                     : todo.priority === 2
                     ? "orange"
                     : "tomato"
               }`,
            }}
         >
            <div className="d-flex align-items-center me-1">
               <span
                  onClick={() => {
                     setUpdateMode(true);
                     setBody(todo.body);
                     setPriority(todo.priority);
                     setTodoId(todo.id);
                     setTodoDone(todo.done);
                  }}
                  className={
                     todo.done ? "ml-1 text-decoration-line-through" : "ms-1"
                  }
               >
                  {todo.body}
               </span>
            </div>
            <div className="d-flex align-items-center justify-content-between">
               <Form.Check
                  key={Math.random()}
                  name="done"
                  value={todo.id}
                  checked={todo.done}
                  onChange={toggleCompleted}
                  type="checkbox"
                  className="me-1"
               ></Form.Check>
               <Trash onClick={(id) => handleDelete(todo.id)} color="tomato" />
            </div>
         </ListGroup.Item>
      );
   };

   return (
      <>
         <Card>
            <Card.Body>
               <Form>
                  <Row>
                     <Col md="9">
                        <Form.Group className="mb-3" controlId="name">
                           <Form.Label className="fw-bold text-black-50">Add Todo</Form.Label>
                           <Form.Control
                              name="body"
                              value={body}
                              onChange={onChange}
                              type="text"
                              placeholder="Todo"
                           />
                        </Form.Group>
                     </Col>
                     <Col>
                        <Form.Group className="mb-3" controlId="name">
                           <Form.Label className="fw-bold text-black-50">Priority</Form.Label>
                           <Form.Select
                              name="priority"
                              value={priority}
                              onChange={onChange}
                           >
                              <option value="1">Low</option>
                              <option value="2">Medium</option>
                              <option value="3">High</option>
                           </Form.Select>
                        </Form.Group>
                     </Col>
                  </Row>

                  <div>
                     <Button
                        size="sm"
                        onClick={updateMode ? handleUpdate : handleSubmit}
                        variant="outline-primary"
                        type="submit"
                     >
                        {updateMode ? "Update" : "Create"}
                     </Button>
                  </div>
               </Form>
               <div className="mt-4 d-flex justify-content-between">
                  <span className="fw-bold text-black-50">Todos (click to edit)</span>
                  <Button
                     onClick={() =>
                        setShowCompleted((showCompleted) => !showCompleted)
                     }
                     size="sm"
                     variant="outline-info"
                  >
                     Show/Hide completed
                  </Button>
               </div> 
               <ListGroup>
                  {showCompleted === false
                     ? todos
                          .filter((todo) => todo.done !== true)
                          .map((todo) => todoListItems(todo))
                     : todos.map((todo) => todoListItems(todo))}
               </ListGroup>
            </Card.Body>
         </Card>
      </>
   );
};

export default Todo;
