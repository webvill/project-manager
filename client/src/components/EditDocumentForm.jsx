import { Button, Card, Form } from "react-bootstrap";
import ReactMarkdown from "react-markdown";

const EditDocumentForm = ({ setEditDocMode, document, setDocument,  handleUpdate }) => {
   function handleChange(e) {
      const { name, value } = e.target;
      setDocument({ ...document, [name]: value });
   }
   const handleSubmit = async (e) => {
      e.preventDefault();
      handleUpdate(document.id)
      setEditDocMode(false);
   };

   return (
      <>
         <Card>
            <Card.Body>
               <Card.Title>Edit document</Card.Title>
               <Form>
                  <Form.Group className="mb-3" controlId="title">
                     <Form.Label>Title</Form.Label>
                     <Form.Control
                        name="title"
                        value={document.title}
                        onChange={handleChange}
                        type="text"
                        placeholder="Title"
                     />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="path">
                     <Form.Label>Path</Form.Label>
                     <Form.Control
                        name="path"
                        value={document.path}
                        onChange={handleChange}
                        type="text"
                        placeholder="Path"
                     />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="body">
                     <Form.Label>Document</Form.Label>
                     <Form.Control
                        as="textarea"
                        rows={3}
                        name="body"
                        value={document.body}
                        onChange={handleChange}
                     />
                  </Form.Group>
                  <div className="d-grid gap-2">
                     <Button variant="outline-success" onClick={handleSubmit}>
                        Update
                     </Button>
                  </div>
               </Form>
               <div className="mt-3">
                  <ReactMarkdown>{document.body}</ReactMarkdown>
               </div>
            </Card.Body>
         </Card>
      </>
   );
};

export default EditDocumentForm;
