import { useState, useEffect } from "react";
import { Form, Button, Card } from "react-bootstrap";
import { useSelector } from "react-redux";
import ReactMarkdown from "react-markdown";

const NewProjectForm = ({ addProject, toggleModal }) => {
   const { token } = useSelector((state) => state.auth);

   const [name, setName] = useState("");
   const [description, setDescription] = useState("");
   const [path, setPath] = useState("");
   const [categories, setCategories] = useState([]);
   const [category, setCategory] = useState("");
   const [selectedStack, setSelectedStack] = useState([]);
   const [stack, setStack] = useState([]);

   const handleCheckboxes = (val) => {
      console.log(val);
      val = parseInt(val);
      const updatedStack = selectedStack.includes(val)
         ? selectedStack.filter((item) => item !== val)
         : selectedStack.concat(val);
      setSelectedStack(updatedStack);
      console.log("selected", selectedStack);
   };

   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(`https://localhost:7000/api/stacks`);
         const data = await response.json();
         console.log("data", data);
         setStack(data);
      };

      fetchData();
   }, []);

   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(`https://localhost:7000/api/categories`);
         const data = await response.json();
         console.log("data", data);
         setCategories(data);
      };

      fetchData();
   }, []);

   const onChange = (e) => {
      const { name, value } = e.target;
      switch (name) {
         case "name":
            setName(value);
            break;
         case "description":
            setDescription(value);
            break;
         case "path":
            setPath(value);
            break;
         case "category":
            setCategory(value);
            break;
         case "stack":
            handleCheckboxes(value);
            break;
         default:
            break;
      }
   };
   const handleSubmit = async (e) => {
      e.preventDefault();
      try {
         const response = await fetch(`https://localhost:7000/api/projects`, {
            method: "POST",
            headers: { "Content-Type": "application/json", "Authorization": `Bearer ${token}` },
            body: JSON.stringify({
               name,
               description,
               path,
               categoryId: category,
               tech: selectedStack,
            }),
         });
            if (response.status === 401) {
               alert("You have to be logged in to create a project")
               toggleModal();
            } else if (response.status !== 201){
               alert('Bad request')
            } else {
               const data = await response.json();
               addProject(data);
               toggleModal();
            }
      
      } catch (error) {
         console.log(error);
      }
   };
   return (
      <Card>
         <Card.Body>
            <Form>
               <Form.Group className="mb-3" controlId="name">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                     name="name"
                     value={name}
                     onChange={onChange}
                     type="text"
                     placeholder="Project name"
                  />
                  <Form.Text className="text-muted">
                     name of the project.
                  </Form.Text>
               </Form.Group>
               <Form.Group className="mb-3" controlId="description">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                     as="textarea"
                     rows={3}
                     name="description"
                     value={description}
                     onChange={onChange}
                  />
                  <ReactMarkdown>{description}</ReactMarkdown>
               </Form.Group>
               <Form.Group className="mb-3" controlId="path">
                  <Form.Label>Path</Form.Label>
                  <Form.Control
                     name="path"
                     value={path}
                     onChange={onChange}
                     type="text"
                     placeholder="Project path"
                  />
               </Form.Group>
               <Form.Group className="mb-3" controlId="category">
                  <Form.Label>Category</Form.Label>
                  <Form.Select
                     name="category"
                     value={category}
                     onChange={onChange}
                  >
                     <option>Select category</option>
                     {categories.map((cat) => (
                        <option key={cat.id} value={cat.id}>
                           {cat.name}
                        </option>
                     ))}
                  </Form.Select>
               </Form.Group>
               <Form.Group className="mb-3">
                  {stack.map((tech) => (
                     <Form.Check
                        key={Math.random()}
                        name="stack"
                        value={tech.id}
                        checked={selectedStack.includes(tech.id)}
                        onChange={onChange}
                        type="checkbox"
                        label={tech.name}
                     />
                  ))}
               </Form.Group>
               <div className="d-grid">
                  <Button
                     onClick={handleSubmit}
                     variant="outline-primary"
                     type="submit"
                  >
                     Create
                  </Button>
               </div>
            </Form>
         </Card.Body>
      </Card>
   );
};

export default NewProjectForm;
