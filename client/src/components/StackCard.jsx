import { Card } from "react-bootstrap";
import { PencilFill, Trash2Fill } from "react-bootstrap-icons";
import { Link } from "react-router-dom";

const StackCard = ({
   stack,
   setStack,
   name,
   stacks,
   editMode,
   setEditMode,
   handleDelete
}) => {
   console.log('stack', stack);
   console.log('name', name);
   console.log('editMode', editMode);
   console.log('stacks', stacks);

   const filteredList = (stacks) => {
      return (
         <Card.Body>
            
            {stacks
               .filter((item) =>
                  item.name.toLowerCase().includes(stack.name.toLowerCase())
               )
               .map((item) => (
                  <div key={item.id} className="d-flex justify-content-between">
                     <span>
                        <Link to={`/stacks/${item.id}`}>{item.name}</Link>
                     </span>
                     <div className="d-flex justify-content-end align-items-center">
                        {editMode && (
                           <span
                              className="fst-italic"
                              onClick={() => {
                                 setEditMode(false);
                                 setStack({id: 0, name:"" });
                                 
                              }}
                           >
                              {item.id === stack.id && (
                                 "...stop editing"
                              )}
                              </span>
                        )}
                           
                        
                           <PencilFill className="me-2"  onClick={() => {
                              setEditMode(true);
                              setStack(item);
                           }}/>
                        
                        <Trash2Fill
                           onClick={() =>
                              handleDelete(item.id)
                           }
                        />
                     </div>
                  </div>
               ))}
         </Card.Body>
      );
   };
   
   return (
         <Card>{!editMode ? filteredList(stacks) : filteredList([stack])}</Card>
   );
};

export default StackCard;
