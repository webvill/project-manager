import { useState } from "react";
import { Button, Form } from "react-bootstrap";
import ReactMarkdown from "react-markdown";

const DocumentForm = ({ tech, setTech, toggleModal, id }) => {
   const [document, setDocument] = useState({ title: "", path: "", body: "" });
   
   function handleChange(e) {
      const { name, value } = e.target;
      setDocument({ ...document, [name]: value });
   }
   const handleSubmit = async (e) => {
      e.preventDefault();
      const response = await fetch(
         `https://localhost:7000/api/stacks/${id}/documents`,
         {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(document),
         }
      );
      const data = await response.json();
      console.log('new posasdfsadft', data);
      setTech({ ...tech, documents: tech.documents.concat(data) });
      setDocument({ title: "", path: "", body: "" });
      toggleModal();
   };

   return (
      <>
         <Form>
            <Form.Group className="mb-3" controlId="title">
               <Form.Label>Title</Form.Label>
               <Form.Control
                  name="title"
                  value={document.title}
                  onChange={handleChange}
                  type="text"
                  placeholder="Title"
               />
            </Form.Group>
            <Form.Group className="mb-3" controlId="path">
               <Form.Label>Path</Form.Label>
               <Form.Control
                  name="path"
                  value={document.path}
                  onChange={handleChange}
                  type="text"
                  placeholder="Path"
               />
            </Form.Group>
            <Form.Group className="mb-3" controlId="body">
               <Form.Label>Document</Form.Label>
               <Form.Control
                  as="textarea"
                  rows={3}
                  name="body"
                  value={document.body}
                  onChange={handleChange}
               />
            </Form.Group>
            <div className="d-grid">
               <Button variant="outline-primary" onClick={handleSubmit}>
                  Save
               </Button>
            </div>
         </Form>
         <div className="mt-3">
            <ReactMarkdown>{document.body}</ReactMarkdown>
         </div>
      </>
   );
};

export default DocumentForm;
