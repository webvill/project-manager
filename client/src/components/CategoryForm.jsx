import { useState } from "react";
import { Form, Button, Card } from "react-bootstrap";
const CategoryForm = () => {
   const [name, setName] = useState("");

   const onChange = (e) => {
      setName(e.target.value);
   };
   const handleSubmit = () => {};
   return (
      <Card>
         <Card.Body>
            <Card.Title>Create a new category</Card.Title>
            <Form>
               <Form.Group className="mb-3" controlId="name">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                     name="name"
                     value={name}
                     onChange={onChange}
                     type="text"
                     placeholder="Category name"
                  />
               </Form.Group>

               <div className="d-grid">
                  <Button
                     onClick={handleSubmit}
                     variant="outline-primary"
                     type="submit"
                  >
                     Create
                  </Button>
               </div>
            </Form>
         </Card.Body>
      </Card>
   );
};

export default CategoryForm;
