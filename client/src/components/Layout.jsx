import { Container, Nav } from "react-bootstrap";
import { Link, Outlet, useNavigate } from "react-router-dom";
import {useSelector, useDispatch } from "react-redux";
import { signOut } from "../features/auth/authThunk";



const Layout = () => {
   const { userData } = useSelector((state) => state.auth);
   console.log('user data', userData);
   const dispatch = useDispatch();
   const navigate = useNavigate();

   const handleSignout = () => {
      dispatch(signOut());
      navigate("/");
   };
   return (
      <Container>
         <Nav activeKey="/" className="justify-content-center">
            <Nav.Item>
               <Nav.Link as={Link} to="/">
                  Home
               </Nav.Link>
            </Nav.Item>
            <Nav.Item>
               <Nav.Link as={Link} to='/stacks'>Stacks</Nav.Link>
            </Nav.Item>
            <Nav.Item>
            {!userData.firstName ? (
               <Nav.Link as={Link} to='/login'>Signin</Nav.Link>
               ) : (

                  <Nav.Link onClick={handleSignout}>Signout {userData.firstName }</Nav.Link>
                  )
               }
            </Nav.Item>
            

         </Nav>
         {<Outlet />}
      </Container>
   );
};

export default Layout;
