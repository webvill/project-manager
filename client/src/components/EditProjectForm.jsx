import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { Form, Button, Card } from "react-bootstrap";

const EditProjectForm = () => {
   const [project, setProject] = useState({
      name: "",
      description: "",
      path: "",
      categoryId: 0,
      tech: [],
      createdAt: "",
      updatedAt: "",
   });
   const [categories, setCategories] = useState([]);
   const [stack, setTech] = useState([]);
   const [selectedTech, setSelectedTech] = useState([]);
   const { id } = useParams();
   const navigate = useNavigate();
   const onChange = (e) => {
      const { name, value } = e.target;
      if (name === "stack") {
         handleCheckboxes(value);
      } else {
         setProject({ ...project, [name]: value });
      }
   };

   const handleCheckboxes = (val) => {
      console.log(val);
      val = parseInt(val);
      const updatedTech = selectedTech.includes(val)
         ? selectedTech.filter((item) => item !== val)
         : selectedTech.concat(val);
      setSelectedTech(updatedTech);
      console.log("selected", selectedTech);
   };

   const handleSubmit = async (e) => {
      e.preventDefault();
      console.log("project", project);
      try {
         const response = await fetch(
            `https://localhost:7000/api/projects/${id}`,
            {
               method: "PUT",
               headers: { "Content-Type": "application/json" },
               body: JSON.stringify({
                  ...project,
                  tech: selectedTech,
               }),
            }
         );
         navigate("/");
         const data = await response.json();
         console.log(data);
      } catch (error) {
         console.log(error);
      }
   };

   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(`https://localhost:7000/api/stacks`);
         const data = await response.json();
         console.log("data", data);
         setTech(data);
      };

      fetchData();
   }, []);

   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(`https://localhost:7000/api/categories`);
         const data = await response.json();
         console.log("data", data);
         setCategories(data);
      };

      fetchData();
   }, []);
   //fetch project
   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(
            `https://localhost:7000/api/projects/${id}`
         );
         const data = await response.json();
         console.log("data", data.tech);
         console.log("project", data);
         setProject(data);
         setSelectedTech(data.tech.map((item) => item.id));
      };

      fetchData();
   }, [id]);

   return (
      <Card>
         <Card.Body>
            <Form>
               <Form.Group className="mb-3" controlId="name">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                     name="name"
                     value={project.name}
                     onChange={onChange}
                     type="text"
                     placeholder="Project name"
                  />
               </Form.Group>
               <Form.Group className="mb-3" controlId="description">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                     as="textarea"
                     rows={3}
                     name="description"
                     value={project.description}
                     onChange={onChange}
                  />
               </Form.Group>
               <Form.Group className="mb-3" controlId="path">
                  <Form.Label>Path</Form.Label>
                  <Form.Control
                     name="path"
                     value={project.path}
                     onChange={onChange}
                     type="text"
                     placeholder="Project path"
                  />
               </Form.Group>
               <Form.Group className="mb-3" controlId="category">
                  <Form.Label>Category</Form.Label>
                  <Form.Select
                     name="categoryId"
                     value={project.categoryId}
                     onChange={onChange}
                  >
                     <option>Select category</option>
                     {categories.map((cat) => (
                        <option key={cat.id} value={cat.id}>
                           {cat.name}
                        </option>
                     ))}
                  </Form.Select>
               </Form.Group>
               <Form.Group className="mb-3">
                  {stack.map((tech) => (
                     <Form.Check
                        key={Math.random()}
                        name="stack"
                        value={tech.id}
                        checked={selectedTech.includes(tech.id)}
                        onChange={onChange}
                        type="checkbox"
                        label={tech.name}
                     />
                  ))}
               </Form.Group>
               <div className="d-grid">
                  <Button
                     onClick={handleSubmit}
                     variant="outline-primary"
                     type="submit"
                  >
                     Update
                  </Button>
               </div>
            </Form>
         </Card.Body>
      </Card>
   );
};

export default EditProjectForm;
