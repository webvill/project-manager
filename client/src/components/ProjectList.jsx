import React, { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import { Link } from "react-router-dom";
import { Button, Form, Modal } from "react-bootstrap";
import { PencilFill, Trash2Fill } from "react-bootstrap-icons";
import NewProjectForm from "./NewProjectForm";
import EditProjectForm from "./EditProjectForm";
const ProjectList = () => {
   const [projects, setProjects] = useState([]);
   const [initialProjects, setInitialProjects] = useState([]);
   const [search, setSearch] = useState("");
   const [showModal, setShowModal] = useState(false);
   const [projectToEdit, setProjectToEdit] = useState({});

   //console.log(projects[0].tech.map((tech) => tech.name.toLocaleLowerCase()));
   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(`https://localhost:7000/api/projects`);
         const data = await response.json();
         console.log("data", data);
         setProjects(data);
         setInitialProjects(data);
      };

      fetchData();
   }, []);

   const filter = (e) => {
      const keyword = e.target.value;
      if (keyword !== "") {
         const results = initialProjects.filter((project) =>
            project.tech
               .map((tech) => tech.name.toLocaleLowerCase())
               .some(
                  (project) => project.indexOf(keyword.toLocaleLowerCase()) > -1
               )
         );
         setProjects(results);
      } else {
         setProjects(initialProjects);
      }
      setSearch(keyword);
   };
   const toggleModal = () => {
      setShowModal(!showModal);
   };

   const addProject = (project) => {
      setProjects([...projects, project]);
      setInitialProjects([...projects, project]);
   };
   const handleDelete = async (id, name) => {
      alert(`Dou you want to delete the ${name} project?`);
      try {
         await fetch(`https://localhost:7000/api/projects/${id}`, {
            method: "DELETE",
         });
         setProjects(projects.filter((project) => project.id !== id));
         setInitialProjects(projects.filter((project) => project.id !== id));
      } catch (error) {}
   };
   const handleEdit = (e, project) => {
      setProjectToEdit(project);
      console.log(project);
   };

   return (
      <>
         <div className="d-flex justify-content-between">
            <Form.Control
               name="search"
               value={search}
               onChange={filter}
               placeholder="filter projects"
               style={{ width: "18rem" }}
            ></Form.Control>
            {/* <Link to="new"> */}
            <Button onClick={toggleModal} variant="outline-success">
               Add new
            </Button>
            {/* </Link> */}
         </div>
         <Table striped bordered hover variant="dark">
            <thead>
               <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Path</th>
                  <th>Stack</th>
                  <th>Actions</th>
               </tr>
            </thead>
            <tbody>
               {projects &&
                  projects.length > 0 &&
                  projects.map((project) => {
                     return (
                        <tr key={project.id}>
                           <td>{project.id}</td>
                           <td>
                              <Link to={`/projects/${project.id}`}>
                                 {project.name}
                              </Link>
                           </td>
                           <td>{project.description}</td>
                           <td>{project.path}</td>
                           <td>
                              {project.tech.map((tech) => tech.name).join(", ")}
                           </td>
                           <td>
                              <Link to={`/projects/${project.id}/edit`}>
                                 <PencilFill className="me-2" />
                              </Link>
                              <Trash2Fill
                                 onClick={(id, name) =>
                                    handleDelete(project.id, project.name)
                                 }
                              />
                           </td>
                        </tr>
                     );
                  })}
            </tbody>
         </Table>
         <Modal show={showModal} onHide={toggleModal}>
            <Modal.Header closeButton>
               <Modal.Title className="text-center">
                  Create a new project
               </Modal.Title>
            </Modal.Header>
            <Modal.Body>
               <NewProjectForm
                  addProject={addProject}
                  toggleModal={toggleModal}
               />
            </Modal.Body>
            <Modal.Footer>
               <Button variant="secondary" onClick={toggleModal}>
                  Close
               </Button>
            </Modal.Footer>
         </Modal>
         {projectToEdit.name && (
            <EditProjectForm
               toggleModal={toggleModal}
               projectToEdit={projectToEdit}
            />
         )}
      </>
   );
};

export default ProjectList;
