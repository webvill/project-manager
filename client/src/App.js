import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Projects from "./pages/projects";
import Layout from "./components/Layout";
import New from "./pages/new";
import Project from "./pages/Project";
import Tech from "./components/Tech";
import EditProjectForm from "./components/EditProjectForm";
import StackForm from "./components/StackForm";
import Login from "./pages/Login";

function App() {
   return (
      <div className="App">
            <Routes>
               <Route path="/" element={<Layout />}>
                  <Route index element={<Projects />} />
                  <Route path="projects/:id" element={<Project />} />
                  <Route path="projects/:id/edit" element={<EditProjectForm />} />
                  <Route path="stacks/:id" element={<Tech />} />
                  <Route path="stacks" element={<StackForm />} />
                  <Route path="new" element={<New />} />
                  <Route path="login" element={<Login />} />
               </Route>
            </Routes>
      </div>
   );
}

export default App;
