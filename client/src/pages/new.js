import { useEffect, useState } from "react";
import CategoryForm from "../components/CategoryForm";
import { Card, Col, Row } from "react-bootstrap";
import StackForm from "../components/StackForm";
const New = () => {
   const [categories, setCategories] = useState([]);
   const [category, setCategory] = useState("");

   const [filteredStack, setFilteredStack] = useState([]);

   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(`https://localhost:7000/api/categories`);
         const data = await response.json();
         console.log("data", data);
         setCategories(data);
      };

      fetchData();
   }, []);

   return (
      <Row className="text-start">
         <Col>
            <CategoryForm />
            <Card>
               <Card.Body>
                  {categories.map((cat) => (
                     <p key={cat.id} className="fw-bold">
                        {cat.name}
                     </p>
                  ))}
               </Card.Body>
            </Card>
         </Col>
         <Col>
            <StackForm />
         </Col>
      </Row>
   );
};

export default New;
