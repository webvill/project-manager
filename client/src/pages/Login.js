import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { getToken } from "../utils/helpers";
import { login } from "../features/auth/authThunk";
import { useSelector, useDispatch } from "react-redux";

const Login = () => {
   const navigate = useNavigate();

   const [username, setUsername] = useState("");
   const [password, setPassword] = useState("");
   const dispatch = useDispatch();

   const { token, loading } = useSelector((state) => state.auth);

   useEffect(() => {
      if (token || getToken()) {
         console.log("redirecting");
         navigate("/");
      }
   }, []);

   const handleLogin = (e) => {
      e.preventDefault();
      dispatch(login({ username, password }));
      navigate("/");
   };

   return (
      <div className="page">
         <div>
            <h2>Login</h2>
         </div>

         <form onSubmit={handleLogin}>
            <div className="mb-3">
               <input
                  className="form-control"
                  onChange={(e) => setUsername(e.target.value)}
                  placeholder="Username"
                  type="text"
                  value={username}
               />
            </div>
            <div className="mb-3">
               <input
                  className="form-control"
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder="Password"
                  type="password"
                  value={password}
               />
            </div>

            {loading ? (
               <div className="loading">
                  <span>Loading...</span>
               </div>
            ) : (
               <button type="submit" className="btn btn-outline-primary">
                  Logga in
               </button>
            )}
         </form>
      </div>
   );
};

export default Login;
