import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import ProjectList from "../components/ProjectList";

const Projects = () => {
   return (
      <>
         <ProjectList />
      </>
   );
};

export default Projects;
