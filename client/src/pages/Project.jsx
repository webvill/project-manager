import { useEffect, useState, useRef } from "react";
import {
   Row,
   Col,
   ListGroup,
   Card,
   Button,
   Modal,
   Form,
} from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import Todo from "../components/Todo";
/* import { Editor, EditorState } from "draft-js";
import "draft-js/dist/Draft.css"; */

const Project = () => {
   const [project, setProject] = useState({
      name: "",
      logs: [],
      tech: [],
      logs: [],
   });
   const [log, setLog] = useState({ day: "", body: "" });
   const [showModal, setShowModal] = useState(false);
   const { id } = useParams();
   const toggleModal = () => {
      setShowModal((showModal) => !showModal);
   };

   const handleChange = (e) => {
      const { name, value } = e.target;
      setLog({ ...log, [name]: value });
   };
   const handleSubmit = async (e) => {
      e.preventDefault();
      try {
         const response = await fetch(
            `https://localhost:7000/api/projects/${id}/log`,
            {
               method: "POST",
               headers: { "Content-Type": "application/json" },
               body: JSON.stringify(log),
            }
         );
         const data = await response.json();
         console.log('new log', data);
         setProject({ ...project, logs: project.logs.concat(data) });
         toggleModal();
      } catch (error) {
         console.log(error);
      }
   };

   useEffect(() => {
      const fetchData = async () => {
         const response = await fetch(
            `https://localhost:7000/api/projects/${id}`
         );
         const data = await response.json();
         console.log("data", data);
         setProject(data);
      };

      fetchData();
   }, [id]);

   return (
      <Row>
         <Col>
            <Card>
               <Card.Body>
                  <div className="d-flex justify-content-between">
                     <span className="card-title text-black-50">{project.name}</span>
                     <Button
                        onClick={toggleModal}
                        variant="outline-primary"
                        size="sm"
                     >
                        new log
                     </Button>
                     <Link to={`/projects/${project.id}/edit`}>Edit</Link>
                  </div>
                  <Card.Text>{project.description}</Card.Text>
                  <Card.Text>
                     <span className="fw-bold text-black-50">Directory:</span>
                     {"  "}
                     {project.path}
                  </Card.Text>
                  <Card.Text>
                     <span className="fw-bold text-black-50">
                        Latest work/update:
                     </span>{" "}
                     {new Date(project.createdAt).toLocaleDateString()}
                  </Card.Text>
                  <h5 className="text-black-50">Stack used</h5>
                  <ListGroup>
                     {project.tech.map((tech) => (
                        <ListGroup.Item key={tech.id}>
                           <Link to={`/stacks/${tech.id}`}>{tech.name}</Link>
                        </ListGroup.Item>
                     ))}
                  </ListGroup>
               </Card.Body>
            </Card>
            {project.logs.length > 0 && (
            <Card>
               <Card.Body>
                  <Card.Title>Logs</Card.Title>
                     {project.logs.map((log) => <p key={log.id}>{log.day}: {log.body}</p>)}
               </Card.Body>
            </Card>
            )}
         </Col>
         <Col>
            <Todo id={id} />
         </Col>
         <Modal show={showModal} onHide={toggleModal}>
            <Modal.Header closeButton>
               <Modal.Title className="text-center">
                  Create a new log entry
               </Modal.Title>
            </Modal.Header>
            <Modal.Body>
               <Card>
                  <Card.Body>
                     <Form>
                        <Form.Group className="mb-3" controlId="name">
                           <Form.Label>Day</Form.Label>
                           <Form.Control
                              name="day"
                              value={log.day}
                              onChange={handleChange}
                              type="text"
                           />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="body">
                           <Form.Label>Body</Form.Label>
                           <Form.Control
                              name="body"
                              value={log.body}
                              onChange={handleChange}
                              type="text"
                           />
                        </Form.Group>

                        <div className="d-grid">
                           <Button
                              onClick={handleSubmit}
                              variant="outline-primary"
                              type="submit"
                           >
                              Create
                           </Button>
                        </div>
                     </Form>
                  </Card.Body>
               </Card>
            </Modal.Body>
            <Modal.Footer></Modal.Footer>
         </Modal>
      </Row>
   );
};

export default Project;
