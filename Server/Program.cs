using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Server.Dtos;
using Server.Models;

var builder = WebApplication.CreateBuilder(args);
// using sqlserver in DataContext
builder.Services.AddDbContext<DataContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
//builder.Services.AddNewtonsoftJson(options=> options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
builder.Services.Configure<JsonSerializerSettings>(options => options.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapGet("/projects", async (DataContext context) =>  await context.Projects.Include(project => project.Tech)
    .Select(project => new {
        Id = project.Id, 
        Name = project.Name, 
        Description = project.Description, 
        Path = project.Path, 
        Log = project.Log, 
        CreatedAt = project.CreatedAt, 
        Tech = project.Tech
    }).ToListAsync());
    
app.MapGet("/projects/{id}", async (DataContext context, int id) => await context.Projects.Include(project => project.Tech)
    .Select(project => new {
        Id = project.Id, 
        Name = project.Name, 
        Description = project.Description, 
        Path = project.Path, 
        Log = project.Log, 
        CreatedAt = project.CreatedAt, 
        Tech = project.Tech
    }).FirstOrDefaultAsync(project => project.Id == id));

app.MapGet("/categories", async (DataContext context) => await context.Categories.ToListAsync());
app.MapGet("/stacks", async (DataContext context) => await context.Tech
 .Select(tech => new {
        Id = tech.Id, 
        Name = tech.Name, 
        Projects = tech.Projects
    }).ToListAsync());

app.MapGet("/stacks/{id}", async (DataContext context, int id) => await context.Tech.Include(tech => tech.Projects)
.Select(tech => new {
        Id = tech.Id, 
        Name = tech.Name, 
        Projects = tech.Projects
    }).FirstOrDefaultAsync(tech => tech.Id == id));

app.MapPost("/projects",   async (DataContext context, CreateProjectDto dto) => {
    
    var project = new Project
    {
        Name = dto.Name,
        Description = dto.Description,
        Path = dto.Path,
        Log = dto.Log,
        CategoryId = dto.CategoryId,
        Tech = context.Tech.Where(item => dto.Tech.Contains(item.Id)).ToList(),
        CreatedAt = dto.CreatedAt,
        UpdatedAt = dto.UpdatedAt

    };
    context.Projects.Add(project);
    await context.SaveChangesAsync();
    return Results.Ok();
});
app.MapPost("/categories",   async (DataContext context, Category category) => {
    context.Categories.Add(category);
    await context.SaveChangesAsync();
    return Results.Ok();
});
app.MapPost("/stacks",   async (DataContext context, Tech tech) => {
    context.Tech.Add(tech);
    await context.SaveChangesAsync();
    return Results.Ok();
});

app.Run();

