using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;

namespace Server.Models
{
    public class DataContext : DbContext
    {
       //protected readonly IConfiguration Configuration;

        public DataContext(DbContextOptions<DataContext> options/* IConfiguration configuration */) : base(options)
        {
            //Configuration = configuration;
        }

        
        /* protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to mysql with connection string from app settings
            //var connectionString = Configuration.GetConnectionString("MyProjectManager");
            //options.UseMySql(connectionString, new MariaDbServerVersion(new Version(10, 3, 12)));
        } */
        public DbSet<Project> Projects {get;set;}/* => Set<Project>(); */
        public DbSet<Category> Categories {get;set;}/* => Set<Category>(); */
        public DbSet<Tech> Tech {get;set;}/* => Set<Tech>(); */
    }
}