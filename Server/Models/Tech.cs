using System.Runtime;
using System.Text.Json.Serialization;

namespace Server.Models
{
    public class Tech
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        [JsonIgnore]
        public List<Project>? Projects { get; set; }
    }
}