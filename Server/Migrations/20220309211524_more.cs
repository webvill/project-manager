﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Server.Migrations
{
    public partial class more : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectTech_Stacks_StacksId",
                table: "ProjectTech");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Stacks",
                table: "Stacks");

            migrationBuilder.RenameTable(
                name: "Stacks",
                newName: "Tech");

            migrationBuilder.RenameColumn(
                name: "StacksId",
                table: "ProjectTech",
                newName: "TechId");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectTech_StacksId",
                table: "ProjectTech",
                newName: "IX_ProjectTech_TechId");

            migrationBuilder.AlterColumn<string>(
                name: "CreatedAt",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(DateTimeOffset),
                oldType: "datetimeoffset");

            migrationBuilder.AddColumn<string>(
                name: "Log",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Path",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "UpdatedAt",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tech",
                table: "Tech",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectTech_Tech_TechId",
                table: "ProjectTech",
                column: "TechId",
                principalTable: "Tech",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectTech_Tech_TechId",
                table: "ProjectTech");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tech",
                table: "Tech");

            migrationBuilder.DropColumn(
                name: "Log",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "Path",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Projects");

            migrationBuilder.RenameTable(
                name: "Tech",
                newName: "Stacks");

            migrationBuilder.RenameColumn(
                name: "TechId",
                table: "ProjectTech",
                newName: "StacksId");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectTech_TechId",
                table: "ProjectTech",
                newName: "IX_ProjectTech_StacksId");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "CreatedAt",
                table: "Projects",
                type: "datetimeoffset",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Stacks",
                table: "Stacks",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectTech_Stacks_StacksId",
                table: "ProjectTech",
                column: "StacksId",
                principalTable: "Stacks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
