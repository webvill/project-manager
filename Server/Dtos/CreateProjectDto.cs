namespace Server.Dtos
{
    public class CreateProjectDto
    {
        public string Name { get; set; } = string.Empty;   
        public string Description { get; set; } = string.Empty;
        public string Log { get; set; } = string.Empty;
        public string Path { get; set; } = string.Empty;
        public int CategoryId { get; set; }
        public List<int>? Tech { get; set; }
        public string CreatedAt { get; set; } = string.Empty;
        public string UpdatedAt { get; set; } = string.Empty;
    }
}