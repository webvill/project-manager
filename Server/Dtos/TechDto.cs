using Server.Models;

namespace Server.Dtos
{
    public class TechDto
    {
        public int Id { get; set; }
        public string Name { get; set; }  = string.Empty;
        public List<Project> Projects { get; set; }
    }
}