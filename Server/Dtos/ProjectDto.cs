using Server.Models;

namespace Server.Dtos
{
    public class ProjectDto
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public string Description { get; set; }
        public string Path { get; set; }
        public int CategoryId { get; set; }
        public List<Tech> Tech { get; set; }
    }
}